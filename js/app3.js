const btnAleatorio = document.getElementById('btnAleatorio');
const btnLimpiar = document.getElementById('btnLimpiar');

btnAleatorio.addEventListener("click", function(){
    
    var arreglo = [];
    var numP=0;
    var numI=0;
    let cantidad = document.getElementById('Cantidad').value;
    arreglo = numAleatorios(arreglo, cantidad);
    arreglo.sort();
    opciones(arreglo);
    simetria(arreglo,numP,numI);
});

btnLimpiar.addEventListener("click",function(){
    var selector = document.getElementById("numeros");
    let pares = document.getElementById('pares');
    let impares = document.getElementById('impares');
    let verifica = document.getElementById('verifica');
    selector.innerHTML=" ";
    pares.innerHTML="Porcentaje de pares: ";
    impares.innerHTML="Porcentaje de impares: ";
    verifica.innerHTML=" ";
    document.getElementById('Cantidad').value = " ";
    var opcion = document.createElement("option");
    opcion.text = "Numeros Aleatorios";
    selector.add(opcion);
});

function numAleatorios(arreglo, cantidad){
    window.alert("Hola");
    for(let x=0; x<cantidad; x++){
        arreglo[x] = Math.round(Math.random()*100);
    }
    return arreglo;
}

function opciones(arreglo){
    var selector = document.getElementById("numeros");
    for(let i = 0; i < arreglo.length; i++){
        var opcion = document.createElement("option");
        opcion.text = arreglo[i];
        opcion.value = arreglo[i].toString();
        selector.add(opcion);
    }  
}

function simetria(arreglo,numP,numI){
    var total;
    for(let x=0;x<arreglo.length;x++){
        if(arreglo[x]%2==0)numP++;
        if(arreglo[x]%2!=0)numI++;
    }
    total = numP+numI;
    numP = numP*100/total;
    numI = numI*100/total;
    let pares = document.getElementById('pares');
    let impares = document.getElementById('impares');
    let verifica = document.getElementById('verifica');
    
    pares.innerHTML="Porcentaje de pares: "+numP+"%";
    impares.innerHTML="Porcentaje de impares: "+numI+"%";
    if((numP >= 50 && numP <=60) || (numI >= 50 && numI <= 60)){
        verifica.innerHTML="Si es Simetrico";
    }
    else{
        verifica.innerHTML="No es Simetrico";
    } 
}

// Manejo de Arrays 

// Declaracion de array con elementos enteros 
let arreglo = [4,89,30,10,34,89,10,5,8,28];

// Diseñar una funcion que recibe como argumento un arreglo 
// de enteros que imprimce cada elemto y el tamaño del areglo

function mostrarArray(arreglo){
    let tamaño = arreglo.length;
    for(let con = 0; con < arreglo.length; ++ con){
        console.log("["+con+"] "+arreglo[con]);
    }
    console.log("tamaño: "+tamaño);

}

// Funcion para mostrar el promedio de los elementos
// de array

function PromedioArray(arreglo){
    let promedio=0;
    for(let x=0;x<arreglo.length;++ x){
        promedio =+ arreglo[x];
    }
    promedio = promedio/arreglo.length;
    console.log("Promedio: "+promedio);
}

// Funcion para mostrar los valores pares de un arreglo

function ValoresParesArray(arreglo){
    let pares = [];
    for(let x=0;x<arreglo.length;x++){

        if(arreglo[x]%2==0)pares.push(arreglo[x]);
    }
    console.log("Pares: "+pares);
}



// Funcion para mostrar el valor mayor de los elementos de un arreglo

function ValorMayorArray(arreglo){
    let mayor=0;
    for(let x=0;x<arreglo.length;x++){
        if(arreglo[x]>mayor){
            mayor = arreglo[x];
        }    
    }
    console.log("El numero mayor es: "+mayor);
}

// Funcion para llenar con valores aleatorios el arreglo.

function ValorAleatorioArray(arreglo){
    let Aleatorio = [];
    let mayor =0;
    for(let x=0;x<100;x++){
        Aleatorio[x] = Math.round((Math.random()*100).toFixed(2));
    }
    for(let x=0;x<100;x++){
        
    }
    console.log("arreglo aleatorio: ");
    console.log(Aleatorio);
    return Aleatorio;
}



// Funcion para mostrar el valor menor de los elementos de un arreglo

function ValorMenorArray(arreglo){
    let menor=arreglo[0];
    for(let x=0;x<arreglo.length;x++){
        if(arreglo[x]<menor){
            menor = arreglo[x];
        }    
    }
    console.log("El numero menor es: "+menor);
}

console.log(mostrarArray(arreglo));
console.log(PromedioArray(arreglo));
console.log(ValoresParesArray(arreglo));
console.log(ValorMayorArray(arreglo));
console.log(ValorMenorArray(arreglo));
ValorAleatorioArray(arreglo);
console.log();
