const btnCalcular= document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');
const datosTbl = document.getElementById('datosTbl');

btnCalcular.addEventListener("click", function(){ //Botón para calcular y obtener valores
    var vali = 0;
    var gen = 0;
    //Obtener los datos de los input
    let peso = document.getElementById('peso').value;
    let estatura = document.getElementById('estatura').value;
    let edad = document.getElementById('edad').value;
    let genero = document.querySelector('input[name="genero"]:checked');
    if(genero) {
        gen = 1;
    } else {
        gen = 0;
    }
    vali = valiEmpty(peso,estatura,edad,gen);
    if(vali == 0){
        alert("No deje ningun espacio vacio");
    }
    else{
        //Hacer los Calculos
        let IMC = peso /(estatura*estatura);
        //Mostrar los datos
        document.getElementById('IMC').value = IMC.toFixed(2);
        //Llamando funciones
        mostrarimg(IMC);
        let calorias = calCalorias(edad,genero,peso);
        mostrarDatos(edad,genero,calorias);  
    } 
});

btnLimpiar.addEventListener("click",function(){ // Botón para Limpiar contenido de imputs
    document.getElementById('peso').value = " ";
    document.getElementById('estatura').value = " ";
    document.getElementById('edad').value = " ";
    let genero = document.querySelector('input[name="genero"]:checked');
    genero.checked = false;
    document.getElementById('IMC').value = " ";
    picture.innerHTML="";
});

function mostrarimg(IMC){ //Función para mostrar imagenes dependiendo del IMC
    var picture = document.getElementById('pict');
    if(IMC < 18.5){
        picture.innerHTML='<img src="/img/01.png">';
    }
    else if(IMC >= 18.5 && IMC <= 24.9){
        picture.innerHTML='<img src="/img/02.png">';
    }
    else if(IMC >= 25 && IMC <= 29.9){
        picture.innerHTML='<img src="/img/03.png">';
    }
    else if(IMC >= 30 && IMC <= 34.9){
        picture.innerHTML='<img src="/img/04.png">';
    }
    else if(IMC >= 35 && IMC <= 39.9 ){
        picture.innerHTML='<img src="/img/05.png">';
    }
    else if(IMC >= 40){
        picture.innerHTML='<img src="/img/06.png">';
    }   
}

function calCalorias(edad,genero,peso){ //Funcion para calcular las calorias que debe consumir dependiendo de la edad y del genero
    if(edad >= 10 && edad < 18){
        if(genero.value == "hombre"){
            return (17.686 * peso + 658.2);
        }
        else{
            return (13.384 * peso + 692.6);
        }
    }
    else if(edad >= 18 && edad < 30){
        if(genero.value == "hombre"){
            return (15.057 * peso + 692.2);
        }
        else{
            
            return (14.818 * peso + 486.6);
        }
    }
    else if(edad >= 30 && edad < 60){
        if(genero.value == "hombre"){
            return (11.472 * peso + 873.1);
        }
        else{
            return (8.1126 * peso + 845.6);
        }
    }
    else if(edad >= 60){
        if(genero.value == "hombre"){
            return (11.711 * peso + 587.7);
        }
        else{
            return (9.082 * peso + 658.5);
        }
    }
}

function mostrarDatos(edad,genero,calorias){ //Función para Mostrar Datos en una tabla dinamica
    let datos = document.getElementById('datosTbl');

    // Crear una nueva fila y celdas
    let fila = datos.insertRow();
    let c1 = fila.insertCell(0);
    let c2 = fila.insertCell(1);
    let c3 = fila.insertCell(2);

    // Asignar contenido a las celdas (puedes usar tus variables IMC, peso, etc.)
    c1.innerHTML = edad;
    c2.innerHTML = genero.value;
    c3.innerHTML = calorias.toFixed(2);
}

function valiEmpty(peso,estatura,edad,gen){ //Función para validar si hay imputs vacios
    if (peso === '' || estatura === '' || edad === '' || gen == 0) {
        return 0;
    }
    else{
        return 1;
    }
}


