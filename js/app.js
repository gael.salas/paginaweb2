const btnCalcular= document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){
    //Obtener los datos de los input

    let valorAuto = document.getElementById('ValorAuto').value;
    let pInicial = document.getElementById('porcentaje').value;
    let plazos = document.getElementById('plazos').value;

    //Hacer los Calculos
    let pagoInicia = valorAuto *(pInicial/100);
    let totalFin = valorAuto-pagoInicia;
    let pagoMensual = totalFin/plazos;

    //Mostrar los datos
    document.getElementById('pagoInicial').value = pagoInicia;
    document.getElementById('totalfin').value = totalFin;
    document.getElementById('pagoMensual').value = pagoMensual;

    let registro = document.getElementById('reg');
    var br = document.createElement("br");
    registro.innerHTML+="Valor del auto: $"+valorAuto+", Porcentaje: "+pInicial+"%, Plazos: "+plazos+", Pago inicial: $"+pagoInicia+", Total a financiar: $"+totalFin+", Pago Mensual: $"+pagoMensual;
    registro.appendChild(br);
    
    

 
});

function limpiar(){
    document.getElementById('ValorAuto').value = "";
    document.getElementById('porcentaje').value = "";
    document.getElementById('plazos').value = "12";
    document.getElementById('pagoInicial').value = "";
    document.getElementById('totalfin').value = "";
    document.getElementById('pagoMensual').value = "";
    
}

