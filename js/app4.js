const datosTbl = document.getElementById('tblAlumno');

let alumno = [{

    "Matricula": "2020030847",
    "Nombre": "Gonzalez Luna Nizarindany",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2020030847.jpg">'
},
{

    "Matricula": "2020030714",
    "Nombre": "Ruiz Guerrero Axel Jovani",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2020030714.jpg">'

},
{
    "Matricula": "2021030008",
    "Nombre": "Landeros Andrade María Estrella",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2021030008.jpg">'

},
{

    "Matricula": "2021030136",
    "Nombre": "Solis Velarde Oscar Alejandro",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2021030136.jpg">'

},
{

    "Matricula": "2020030550",
    "Nombre": "Flores Perez Jesus Adolfo",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2020030550.jpg">'

},
{

    "Matricula": "2021030266",
    "Nombre": "Gonzalez Ramirez Jose Manuel",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2021030266.jpg">'

},
{

    "Matricula": "2019030391",
    "Nombre": "Salazar Bustamante Alan Arturo",
    "Grupo": "7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2019030391.jpg">'

},
{
    
    "Matricula": "2021030118",
    "Nombre": "Ibarra Hernández Ángel Antonio",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2021030118.jpg">'

},
{
    
    "Matricula": "2020030321",
    "Nombre": "Ontiveros Govea Yair Alejandro ",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2020030321.jpg">'

},
{

    "Matricula": "2021030262",
    "Nombre": "Qui Mora Ángel Ernesto",
    "Grupo": "TI 7-3",
    "Carrera": "Tecnologias de la Informacion",
    "Foto": '<img src="/img/img_Alumnos/2021030262.jpg">'

},
];

let datos = document.getElementById('tblAlumno');

// Crear una nueva fila y celdas



// Asignar contenido a las celdas
for(let x=0;x<alumno.length;x++){
    let fila = datos.insertRow();
    let c1 = fila.insertCell(0);
    let c2 = fila.insertCell(1);
    let c3 = fila.insertCell(2);
    let c4 = fila.insertCell(3);
    let c5 = fila.insertCell(4);
    c1.innerHTML = alumno[x].Matricula;
    c2.innerHTML = alumno[x].Nombre;
    c3.innerHTML = alumno[x].Grupo;
    c4.innerHTML = alumno[x].Carrera;
    c5.innerHTML = alumno[x].Foto;
}




/*
for(let j=0; j<alumno.length; j++){

    console.log("Matricula" + alumno[j].Matricula);
    console.log("Nombre: " + alumno[j].Nombre);
    console.log("Grupo: " + alumno[j].Grupo);
    console.log("Carrera: " + alumno[j].Carrera);
    console.log("Foto: " + alumno[j].Foto);
    console.log("-----------------------------");

}

console.log("Matricula: " + alumno.Matricula);
console.log("Nombre: " + alumno.Nombre);

alumno.Nombre = "Nizarindany Luna";
console.log("Nuevo Nombre: " + alumno.Nombre);


//Objetos Compuestos

let cuentaBanco = {

    "numero": "10001",
    "banco": "BANCOMER",
    cliente: {
        "Nombre": "Jose Madero",
        "fechaNac": "1980/30/11",
        "Sexo": "M"

    },

    "saldo": "10000"
}

console.log("Nombre: " + cuentaBanco.cliente.Nombre);
console.log("Saldo: " + cuentaBanco.saldo);
cuentaBanco.cliente.Sexo = 'F';
console.log(cuentaBanco.cliente.Sexo);

//arreglo de productos

let productos = [
    {
    "codigo":"1001",
    "descripcion":"Atun",
    "precio":"34"},
    {
    "codigo":"1002",
    "descripcion":"Jabon",
    "precio":"23"},
    {
    "codigo":"1003",
    "descripcion":"Harina",
    "precio":"43"},
    {
    "codigo":"1004",
    "descripcion":"Pasta dental",
    "precio":"78"}
]

//Mostrar un atributo de un onjeto en un arreglo

console.log("La descripcion es " + productos[0].descripcion);

for(let i=0; i<productos.length; i++){

    console.log("Codigo: " + productos[i].codigo);
    console.log("Descripcion: " + productos[i].descripcion);
    console.log("Precio: " + productos[i].precio);
    console.log("------------------------------");

}
*/